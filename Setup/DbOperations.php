<?php

namespace Shaamaan\Lesson1\Setup;

use Magento\Framework\DB\Ddl\Table;

class DbOperations
{

    public function createLatestSchema(\Magento\Framework\Setup\SetupInterface $setup)
    {
        $tableDef = $setup->getConnection()->newTable($setup->getTable(\Shaamaan\Lesson1\Api\Data\LessonDataInterface::DEFAULT_SCHEMA_NAME));
        //wnoiski: trzeba (chyba?) ustawić wszystko ręcznie
        $tableDef->addColumn(
            \Shaamaan\Lesson1\Api\Data\LessonDataInterface::FIELD_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true]
        );
        $tableDef->addColumn(\Shaamaan\Lesson1\Api\Data\LessonDataInterface::FIELD_DATA, Table::TYPE_TEXT, 200);

        $setup->getConnection()->createTable($tableDef);
    }

    public function upgradeFrom0_1_0($setup)
    {
        //dont do antyhing yet
    }
}