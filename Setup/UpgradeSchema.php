<?php
/**
 * Created by PhpStorm.
 * User: Shaamaan
 * Date: 22.05.2018
 * Time: 20:22
 */

namespace Shaamaan\Lesson1\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /** @var DbOperations */
    private $dbOperations;

    public function __construct(DbOperations $dbOperations) {
        $this->dbOperations = $dbOperations;
    }


    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        //sprawdzenie po wersji
        if (version_compare($context->getVersion(), '0.1.0', '<')) {
            $this->dbOperations->upgradeFrom0_1_0($setup);
        }
    }

}