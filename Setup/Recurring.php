<?php
/**
 * Created by PhpStorm.
 * User: Shaamaan
 * Date: 22.05.2018
 * Time: 20:22
 */

namespace Shaamaan\Lesson1\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Shaamaan\Lesson1\Api\Data\LessonDataInterface;

//this will run always - use to check if db exists
class Recurring implements InstallSchemaInterface
{
    /** @var DbOperations */
    private $dbOperations;
    public function __construct(DbOperations $dbOperations)
    {
        $this->dbOperations = $dbOperations;
    }


    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        //make a check
        if (!$setup->tableExists(LessonDataInterface::DEFAULT_SCHEMA_NAME)) {
            $setup->startSetup();

            $this->dbOperations->createLatestSchema($setup);

            $setup->endSetup();
        }
    }

}