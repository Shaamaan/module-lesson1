<?php
//w teorii to powinno sprawdzać czy typy zadeklarowane i zwracane są zgodne (runtime)
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Shaamaan
 * Date: 29.05.2018
 * Time: 19:11
 */

namespace Shaamaan\Lesson1\Block;


class MyForm extends \Magento\Framework\View\Element\Template
{

    private $defaultLogic;
    private $url;

    /**
     * MyDefault constructor.
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Shaamaan\Lesson1\Model\DefaultLogic $defaultLogic,
        \Magento\Framework\UrlInterface $url
    )
    {
        parent::__construct($context, $data);
        $this->defaultLogic = $defaultLogic;
        $this->url = $url;
    }

    public function getAddUrl() : string
    {
        //* - zwraca ten element, z którego to jest wywoływane
        return $this->url->getUrl('*/*/add');
        //powyzsze zwroci lesson1(lub ex1)/index/add
    }

}