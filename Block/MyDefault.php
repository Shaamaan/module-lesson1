<?php

namespace Shaamaan\Lesson1\Block;


use Magento\Framework\View\Element\Template;

class MyDefault extends \Magento\Framework\View\Element\Template
{

    private $defaultLogic;
    /**
     * MyDefault constructor.
     */
    public function __construct(Template\Context $context, array $data = [],
                                \Shaamaan\Lesson1\Model\DefaultLogic $defaultLogic)
    {
        parent::__construct($context, $data);
        $this->defaultLogic = $defaultLogic;
    }


    public function getName()
    {
        return $this->defaultLogic->getCustomerName();
    }
}