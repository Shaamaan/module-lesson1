<?php

namespace Shaamaan\Lesson1\Model\ResourceModel\LessonData;


use Shaamaan\Lesson1\Model\ResourceModel\LessonData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Shaamaan\Lesson1\Model\LessonData::class, LessonData::class);
    }
}