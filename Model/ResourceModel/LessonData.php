<?php
/**
 * Created by PhpStorm.
 * User: Shaamaan
 * Date: 22.05.2018
 * Time: 22:04
 */

namespace Shaamaan\Lesson1\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LessonData extends AbstractDb
{
    protected function _construct()
    {
        $this->_init(\Shaamaan\Lesson1\Api\Data\LessonDataInterface::DEFAULT_SCHEMA_NAME, \Shaamaan\Lesson1\Api\Data\LessonDataInterface::FIELD_ID);
    }
}