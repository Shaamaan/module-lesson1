<?php
/**
 * Created by PhpStorm.
 * User: Shaamaan
 * Date: 22.05.2018
 * Time: 22:02
 */

namespace Shaamaan\Lesson1\Model;


use Magento\Framework\Model\AbstractModel;
use Shaamaan\Lesson1\Api\Data\LessonDataInterface;

class LessonData extends AbstractModel implements \Shaamaan\Lesson1\Api\Data\LessonDataInterface
{
    /**
     * MySchema constructor.
     */
    public function _construct() //fuck up! to nie jest KONSTRUKTOR konstruktor... to ma jeden podkreślnik!
    {
        $this->_init(ResourceModel\LessonData::class);
    }

    function getLessonData(): ?string
    {
        return $this->_getData(LessonDataInterface::FIELD_DATA);
    }

    function setLessonData(?string $data): LessonDataInterface
    {
        $this->setData(LessonDataInterface::FIELD_DATA, $data);
        return $this;
    }

    function getLessonDataId(): ?int
    {
        return $this->_getData(LessonDataInterface::FIELD_ID);
    }

    function setLessonDataId(?int $id): LessonDataInterface
    {
        $this->setData(LessonDataInterface::FIELD_ID, $id);
        return $this;
    }


}