<?php

namespace Shaamaan\Lesson1\Model;


use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;

class DefaultLogic
{
    private $customerRepository;
    private $searchCriteriaBuilder;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getCustomerName()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('email', "roni%", 'like')->setPageSize(1)->create();
        $results = $this->customerRepository->getList($searchCriteria);
        $items = $results->getItems();

        if ($results->getTotalCount() === 0)
            throw new LocalizedException(__('Default customer not found.'));    //__() <- taki wynalazek do tłumaczeń

//        $shittyunumber = '123,123'; //ktoś jest kurwa z polski...
//        $shittyunumber = str_replace(',','.', $shittyunumber);
//        $shittyunumber = (double)$shittyunumber;

        $firstElement = $items[0]; //Yo, Szatan, to też działa!

//        $firstElement = array_shift( $items ); //<- this also gets the first element of an array
        $fullName = $firstElement->getFirstname() . ' ' . $firstElement->getLastname(); // kropka jako łączenie - tak, to jest kropka...
        return $fullName;
    }
}