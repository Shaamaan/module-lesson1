<?php

namespace Shaamaan\Lesson1\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Shaamaan\Lesson1\Api\Data\LessonDataInterface;
use Shaamaan\Lesson1\Api\LessonDataRepositoryInterface;
use \Shaamaan\Lesson1\Model\ResourceModel\LessonData as LessonDataResource;
use Magento\Framework\Api\SearchResultsInterface;

class LessonDataRepository implements LessonDataRepositoryInterface
{
    protected $resource;

    protected $lessonDataFactory;

    protected $mySchemaCollectionFactory;

    protected $searchResultsFactory;

    private $collectionProcessor;

    public function __construct(
        LessonDataResource $resource,
        \Shaamaan\Lesson1\Model\LessonDataFactory $lessonDataFactory,
        \Shaamaan\Lesson1\Model\ResourceModel\LessonData\CollectionFactory $lessonDataCollectionFactory,
        SearchResultsInterface $searchResultsFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor = null
    )
    {
        $this->resource = $resource;
        $this->lessonDataFactory = $lessonDataFactory;
        $this->mySchemaCollectionFactory = $lessonDataCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(LessonDataInterface $lessonData)
    {
        try {
            $this->resource->save($lessonData);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the schema: %1', $exception->getMessage()),
                $exception
            );
        }
        return $lessonData;
    }


    public function getById($dataId)
    {
        $mySchema = $this->lessonDataFactory->create();
        $mySchema->load($dataId);
        if (!$mySchema->getId()) {
            throw new NoSuchEntityException(__('Schema with id "%1" does not exist.', $dataId));
        }
        return $mySchema;
    }


    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->mySchemaCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function delete(LessonDataInterface $lessonData)
    {
        try {
            $this->resource->delete($lessonData);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the schema: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    public function deleteById($dataId)
    {
        return $this->delete($this->getById($dataId));
    }

}