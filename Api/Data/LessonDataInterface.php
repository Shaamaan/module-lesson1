<?php

namespace Shaamaan\Lesson1\Api\Data;


interface LessonDataInterface
{
    const FIELD_ID = 'lessondata_id'; //konwencja dla ID : nazwa schematu_id
    const FIELD_DATA = 'lesson_data'; //konwencja dla kolumn: wyrazy oddzielone _

    const DEFAULT_SCHEMA_NAME = 'shaamaan_lesson1_lessonData';

    function getLessonDataId(): ?int;

    function getLessonData(): ?string;

    function setLessonDataId(?int $id): LessonDataInterface;

    function setLessonData(?string $data): LessonDataInterface;
}