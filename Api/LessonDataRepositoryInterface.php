<?php

namespace Shaamaan\Lesson1\Api;

use Shaamaan\Lesson1\Api\Data\LessonDataInterface;

interface LessonDataRepositoryInterface
{
    public function save(LessonDataInterface $lessonData);

    public function getById($dataId);

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria);

    public function delete(LessonDataInterface $lessonData);

    public function deleteById($dataId);
}