<?php
namespace Shaamaan\Lesson1\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use function PHPSTORM_META\type;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page resultPage */
        $resultPage = $this->resultFactory->create( ResultFactory::TYPE_PAGE);

        return $resultPage;
    }


}
